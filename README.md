I have created automation suite to test Swagger Petstore API https://petstore.swagger.io/

**Tools/libraries used**

Java
Rest Assured
Cucumber
JUnit
Maven

**Installation**

You will need:
- Java 1.8+ installed (I used version jdk 13.0.1 )
- Maven Installed (I use version 3.5.2) 
- Eclipse (Or another Java IDE)



**Petstore Swagger assignment using RestAssured and Cucumber BDD**

This repository contains a collection of RestAssured(Eclipse IDE) project and libraries that demonstrate how to use the tool and develop automation script using the Cucumber BDD framework.

**Installation**

_Easy setup by Cloning the project_

Clone / Download the project into your local machine and then import as maven project in the eclipse.
To do this, open a new workspace in Eclipse and click on import project, choose maven project and browse the cloned/downloaded project in the workspace.

**Open the Command prompt and navigate to project location**

Right click and run as Junit Test
/*Execute the following Maven command's
mvn clean :- To clean the maven repo
mvn install :- To install the maven requirments
mvn test :- To execute the test scenarios*/

If the above steps/cloning do not work then kindly install the dependencies as mentioned in pom.xml file
OR follow the steps as described below.
To install this project in your system, you will need Selenium IDE driver, JDK 
Pre-requisites

JAVA: Install JAVA(v1.8) and make sure JAVA_HOME is set properly in your system environment variable.You can download java from the site- https://www.java.com/en/
Selenium IDE 
Extent Reports for reporting : It is used for HTML reporting. 


**Selenium IDE**
To run your test you must have selenium server up and running to execute any  tests, or it will fail fast with an error.


**Steps for Installation**

Clone / Download the project into your local machine and then it is need to import this maven project into eclipse.
Open a new workspace in Eclipse and click on import project, choose maven project and browse the cloned/downloaded project in the workspace.

All needed dependencies can be found in pom.xml file of the project.

In this project I created five packages in src/test/java
1. For pojo class. POJO stands for plain old java object. In this package I created 8 classes.
2.For API's. In this package I kept all my api end points like petapi, storeapi, userapi.
3. For runner. In this package I includes RunCuke class in which I defined steps for gluing feature and step definition file in cucumber BDD framework .I also defined a plugin for report generating.
4.For step definition for my features files I created this package in ehich I defined steps for my feature file.
5. A BaseTest class in which I configured my BaseURI and Basepath to be called globally. 


Now, In src/test/resources I created 
1. Features file
2. config properties in Properties folder which contains BaseURI and Basepath
3. log4j properties file 


**Run your test scripts**
To run your test scripts, you can right click and run as junit test. 
or
In command promt, after directing to the project path you can write _mvn test_ to run the test.


To add new feature, Create a new feature file in folder src/test/resources/features. The name of file should be in small letter with extension .feature
Write scenario outline, When and Then statements. If some test case has to be executed with different set of test data then send test as parameters as shown in below example.
First line should contain name of fields, from second line, start inserting test data.

Scenario Outline: Find pet by Status
	 Given When URI is available
	 When User send request to find pet by Status "<Status>"
	 And  check if response get status code 200
	
	Examples: 
	|Status|
	|Available| 
	
There are 3 step definition files in folder .src/test/java/com/petstore/APITesting/Steps/step-definitions. (createUserStepdefinition, petStepdefinition, storePetStepdefinition)
For each steps described in feature file, check if the step definitions are present in these file. If step definitions are not present, it should be added here.
In step definition file,
a) import the module of page class file.
b) create a object of the page class
c) call the methods to interact with the elements on the page.
In folder pageobjects, in the class file define methods to interact with elements of the page.

Example :
To add new  scenario
Scenario Outline: Find pet by petID
	 Given When URI is available
	 When User send request to find pet by <petId>
	 And  check if response get status code 200
	 
	 
	Examples: 
	|petId|
	|1278| 
	
Perform following steps :
Create new feature file with following text in it or add test scenarios in existing feature file which ever applicable.

Step definition for step "When URI is available" is already present in createUser step definitions.
Step definition for step " User send request to find pet by <petId>" are not present in the step definition. Add the step definition of this step in pet step definition file 
Step definition for step  check if response get status code 200 is already present in current step definitions.

If more assertions are required, then add steps accordingly.

HTML Reporting:- HTML reports are being generated under target folder and can be viewed by clicking on file under target folder by alt+enter which will redirect you to path of generated report and then report can be viewed.
