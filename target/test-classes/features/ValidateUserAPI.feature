Feature: Validate user operations
I want to validate all the user operation through APIs

@dontexecute
Scenario Outline: To create new user by passing user data
	#Given User send request to api_url
	Given When URI is available
	When User pass "<Id>","<userName>","<firstName>","<lastName>","<Email>","<Password>","<Phone>","<userStatus>"
	Then check if a new user is created "<Id>"
	And check if response get status code 200

	Examples: 
	|Id|userName|firstName|lastName|Email|Password|Phone|userStatus|
	|451|UsPetLover1|Usha|Pascal|ushapascal@abc.com|password@123|9087653312|-674944|
@dontexecute
Scenario Outline: To check if a user can login with valid username and password
	#Given User send request to api_url
	Given When URI is available
	When the user logs in the system using "<username>","<password>"
    Then check if User has logged in the system
	And check if response get status code 200

	Examples: 
	|username|password|
	|UsPetLover1|password123|
@dontexecute
Scenario Outline: Update users data when user exists
	Given When URI is available
	When the user logs in the system using "UsPetLover1","password@123"
	And the user update the data "<Id>","<userName>","<firstName>","<lastName>","<Email>","<Password>","<Phone>","<userStatus>"
    Then the userdata is updated "<Id>"
        
        Examples: 
	|Id|userName|firstName|lastName|Email|Password|Phone|userStatus|
	|1234568923|UsPetLover2|Usha|Pascal|ushapascal@test.com|password@123|9087653312|-674944|

#Scenario Outline: Update users data when user does not exists
	#Given When URI is available
	#When the user logs in the system using "UsPetLover1","password@123"
    #And the user update the data "<Id>","<userName>","<firstName>","<lastName>","<Email>","<Password>","<Phone>","<userStatus>"
    #Then the userdata is not updated
        
     #   Examples: 
#	|Id|userName|firstName|lastName|Email|Password|Phone|userStatus|
#	|1234568923|XXX93849|Usha|Pascal|ushapascal@test.com|password@123|9087653312|-989898|
@dontexecute	
Scenario Outline: Get user data when user exists
	Given When URI is available
	When the user logs in the system using "UsPetLover1","password@123"
    And the logged-in user gets other user details "<userName>"
    Then check if user data is correct "<Id>","<userName>","<firstName>","<lastName>","<Email>","<Password>","<Phone>","<userStatus>"
        
        Examples: 
	|Id|userName|firstName|lastName|Email|Password|Phone|userStatus|
	|451|UsPetLover1|Usha|Pascal|ushapascal@abc.com|password@123|9087653312|-989898|

Scenario Outline: Get user data when user does not exists
	Given When URI is available
	When the user logs in the system using "UsPetLover1","password@123"
	And the logged-in user gets other user details "<userName>"
    Then check if response body contains "<code>","<type>","<message>"
    
        
        Examples: 
	|userName|code|type|message|
	|UsPetLover6|1|error|User not found|

Scenario Outline: Logout user
        Given When URI is available	
        When the user logs in the system using "<username>","<password>"
        When user logs out
        Then check if response body contains "<code>","<type>","<message>"
               
        
        Examples: 
			|userName|Password|code|type|message|
			|UsPetLover1|password@123|200|unknown|ok|
			
Scenario Outline: Delete an non- existing user
        Given When URI is available	
        When the user logs in the system using "<username>","<password>"
        When the user delete the data "<userName>"
        Then check if response get status code 404
        
        Examples:
        |userName|Password|
		|UsPet|password@123|	

Scenario Outline: Delete an existing user
        Given When URI is available	
        When User pass "<Id>","<userName>","<firstName>","<lastName>","<Email>","<Password>","<Phone>","<userStatus>"
        And the user delete the data "<userName>"
        Then check if response get status code 200
         Then check if response body contains "200","unknown","<userName>"
        
        Examples: 
		|Id|userName|firstName|lastName|Email|Password|Phone|userStatus|
		|452|DelUsPetLover|Deluser|Pascal|ushapascal@abc.com|password@123|9087653312|-674944|
