Feature: Validate store operations
I want to validate all the access to petstore operation through APIs


Scenario Outline:To place an order for purchasing the pet
	 Given When URI is available
	 When  place an order for purchasing pet "<Id>","<petId>","<quantity>","<shipDate>","<status>","<complete>"
	 And   check if response get status code 200

	Examples: 
	|Id|petId|quantity|shipDate|status|complete|
	|45145|784596|345|2002-09-05T14:49:38.805Z|delivered|true|
	
Scenario Outline: To find purchase order by Id
	 Given When URI is available
	 When  user send Petorder id "<Id>"
	 And  check if response get status code 200
	 Then check if purchase order data is correct "<Id>","<petId>","<quantity>","<shipDate>","<status>","<complete>"
	 
	 Examples:
	 |Id|petId|quantity|shipDate|status|complete|
	 |45145|784596|345|2002-09-05T14:49:38.805+0000|delivered|true|

Scenario Outline: Delete purchase order by Id for existing order
	 Given When URI is available
	 When  place an order for purchasing pet "<Id>","<petId>","<quantity>","<shipDate>","<status>","<complete>"
	 And  user delete "<Id>"
	 Then check if response body contains "<code>","<type>","<message>"
	 And   check if response get status code 200
	 
	 Examples:
	 |Id|petId|quantity|shipDate|status|complete|code|type|message|
	 |45145|784596|345|2002-09-05T14:49:38.805Z|delivered|true|200|unknown|45145|
	 
Scenario Outline: Returns pet inventories by status
	 Given When URI is available
	 When user request pet inventory for each status code
	 Then  check if response get status code 200
	 Examples:
	 |statuscode|
	 |200|
	 	 	 
Scenario Outline: Delete purchase order by Id for non-existing order
	 Given When URI is available
	 When  user delete "<Id>"
	 Then check if response body contains "<code>","<type>","<message>"
	 And   check if response get status code 404
	 
	 Examples:
	 |Id|code|type|message|
	 |889|404|unknown|Order Not Found|
	 