Feature: Validate Pet operations
I want to validate all the pets operation through APIs


Scenario Outline: Add a new pet to the store
	 Given When URI is available
	 When User sends new pet details  to add to the store <id>,"<name>","<photourl>",<catId>,"<catName>",<tagId1>,"<tagname1>","<status>"
	  And  check if response get status code 200
	
	 

	Examples: 
	|name|photoUrls|id|catId|catName|tagId1|tagname1|status|
	|Tommy|Url1|123|1|catname1|23|df|available|
	
Scenario Outline: Update an existing pet to the store
	 Given When URI is available
	 When User sends new pet details  to add to the store <id>,"<name>","<photourl>",<catId>,"<catName>",<tagId1>,"<tagname1>","<status>"
	 Then User updates pet details <id>,"<name>","<photourl>",<catId>,"<catName>",<tagId1>,"<tagname1>","<status>"
	  And  check if response get status code 200
	 
	
     Examples: 
	|name|photoUrls|id|catId|catName|tagId1|tagname1|status|
	|Winnie|Url45|1278|6|catname34|232|dfe|available|
	

	

Scenario Outline: Find pet by Status
	 Given When URI is available
	 When User send request to find pet by Status "<Status>"
	 #Then pet details are returned
	  And  check if response get status code 200
	
	Examples: 
	|Status|
	|Available| 
	

Scenario Outline: Find pet by petID
	 Given When URI is available
	 When User send request to find pet by <petId>
	 And  check if response get status code 200
	 
	
	Examples: 
	|petId|
	|1278| 
	
Scenario Outline: Update pet by petID
	 Given When URI is available
	 When User updates <petId>,"<name>", "<status>"
	 Then check if response body contains "<code>","<type>","<message>"
	 
	
	Examples: 
	|petId|name|status|code|type|message|
	|1278|kikku|sold|200|unknown|1278|
	
Scenario Outline: Delete existent pet by petID
	 Given When URI is available
	 When User send request to delete pet by <petId>
	 Then check if response body contains "<code>","<type>","<message>"
	 
	  Examples: 
	|petId|code|type|message|
	|1278|200|unknown|1278|
	
Scenario Outline: Delete non-existent pet by petID
	 Given When URI is available
	 When User send request to delete pet by <petId>
	 And check if response get status code 404
	 
	
	
	Examples: 
	|petId|
	|498| 