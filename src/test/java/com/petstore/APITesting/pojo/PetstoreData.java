package com.petstore.APITesting.pojo;

import java.util.ArrayList;

public class PetstoreData {
	
	private ArrayList<UserData> user;

	public PetstoreData(ArrayList <UserData> user) {
		this.user=user;
		
	}

	public ArrayList<UserData> getUser() {
		return user;
	}

	public void setUser(ArrayList<UserData> user) {
		this.user = user;
	}

}
