package com.petstore.APITesting.pojo;


import java.util.ArrayList;

public class PetData {
            private int id;
            private String name;
            private ArrayList<String> photoUrls;
            private PetCategory category;
            private ArrayList<PetTags> tags;
            private String status;
            
                 
            public PetData(int id,String name,ArrayList<String> urlList,int catId,String catName,ArrayList <PetTags> tag,String status) {
            	this.id=id;
            	this.name=name;
            	this.photoUrls = urlList;
            	this.category= new PetCategory(catId,catName);
            	this.tags = tag;
            	this.status = status;
            	
            }


			public int getId() {
				return id;
			}


			public void setId(int id) {
				this.id = id;
			}


			public String getName() {
				return name;
			}


			public void setName(String name) {
				this.name = name;
			}


			public ArrayList<String> getPhotoUrls() {
				return photoUrls;
			}


			public void setPhotoUrls(ArrayList<String> photoUrls) {
				this.photoUrls = photoUrls;
			}


			public PetCategory getCategory() {
				return category;
			}


			public void setCategory(PetCategory category) {
				this.category = category;
			}


			public ArrayList<PetTags> getTags() {
				return tags;
			}


			public void setTags(ArrayList<PetTags> tags) {
				this.tags = tags;
			}


			public String getStatus() {
				return status;
			}


			public void setStatus(String status) {
				this.status = status;
			}
            
}
