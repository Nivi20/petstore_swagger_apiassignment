package com.petstore.APITestingFramework.cuke.runner;



import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;

//import org.junit.runner.RunWith;



//import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;



@RunWith(Cucumber.class)
@CucumberOptions(
		          features= {"src/test/resources/features"},
                  glue= {"com.petstore.APITestingFramework.cuke.Steps"},
                  monochrome = false,
                  plugin = {
                		   "pretty", "html:target/MyReports/report.html",
                			"json:target/cucumber-report/runwebat/cucumber.json",
                			"rerun:target/cucumber-report/runwebat/rerun.txt",
                			
                			}
                  
                 

)


public class RunCukeTest extends AbstractTestNGCucumberTests{
	
	
	   
	}

	
	
	
	


