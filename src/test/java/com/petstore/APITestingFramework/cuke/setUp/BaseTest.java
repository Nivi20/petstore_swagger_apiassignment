package com.petstore.APITestingFramework.cuke.setUp;

    import java.io.FileInputStream;
	import java.io.FileNotFoundException;
	import java.io.IOException;
	import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import io.restassured.RestAssured;
import io.restassured.response.Response;
//import io.restassured.response.Response;

	public class BaseTest {

		public static Properties config = new Properties();
		private static FileInputStream fis;
		//public static Object response;
		public static Response Response;
		public static Logger logger;
		
		

		
		@BeforeSuite
		public static void setup()  {
			Logger logger = Logger.getLogger("Logger_File");
			String OS = System.getProperty("os.name").toLowerCase();
			String log4jPath;
			String configPath;
			if(OS.contains("win")) {
				log4jPath = System.getProperty("user.dir") + "\\src\\test\\resources\\properties\\log4j.properties";
				configPath = ".\\src\\test\\resources\\properties\\config.properties";
			}
			else {
				log4jPath = System.getProperty("user.dir") + "/src/test/resources/properties/log4j.properties";
				configPath = "src/test/resources/properties/config.properties";
			}
			
			PropertyConfigurator.configure(log4jPath);
			try {
				fis = new FileInputStream(configPath);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				try {
					config.load(fis);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				RestAssured.baseURI=config.getProperty("baseURI");
				RestAssured.basePath=config.getProperty("basePath");
				
				
			}
			
		
		
		@AfterSuite
		public void tearDown() {
			
			
			
	      
			
			
		}

	}

