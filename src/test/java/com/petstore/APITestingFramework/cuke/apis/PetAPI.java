package com.petstore.APITestingFramework.cuke.apis;

import com.petstore.APITesting.pojo.PetData;
//import com.petstore.APITesting.pojo.StoreData;

//import groovy.json.JsonSlurper;
//import io.cucumber.java.Status;
//import io.cucumber.java.hu.De;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class PetAPI {

	
	public static Response orderPet(PetData data) {
	    
   		
		 Response Response = RestAssured
        		           .given()
                           .contentType(ContentType.JSON)
                            .body(data)
                           .log()
                           .everything().post("/pet");
                
     Response.prettyPrint();
    System.out.println(Response.getStatusCode());
    //System.out.println("Response is: "+Response.asString());
    //String orderId = Response.jsonPath().get("id").toString();
   
		return Response;
		 }

	public static Response updatePet(PetData data) {
		Response Response = RestAssured
		           .given()
                .contentType(ContentType.JSON)
                 .body(data)
                .log()
                .everything().post("/pet");
		
		Response.prettyPrint();
	    System.out.println(Response.getStatusCode());
	    return Response;
	}
	
	public static Response findPetByStatus(String status) {
		Response Response = RestAssured
		           .given()
                   .param("status", status)
                   .log()
                .everything().get("/pet/findByStatus");
		
		
		Response.prettyPrint();
	    System.out.println(Response.getStatusCode());
	    return Response;
	}		

	public static Response findPetBypetId(int petId) {
		Response Response = RestAssured
		           .given()
                   .param("petId", petId)
                   .log()
                .everything().get("/pet"+"/"+petId);
		
		
		Response.prettyPrint();
	    System.out.println(Response.getStatusCode());
	    return Response;	
		
		
	}
	
	public static Response updatePetBypetId(int petId, String name, String status) {
		Response Response = RestAssured
		           .given()
		           .param("petId", petId)
                   .log()
                .everything().post("/pet"+"/"+petId);
		
		
		Response.prettyPrint();
	    System.out.println(Response.getStatusCode());
	    return Response;	
		
		
	}
	
	public static Response deletePetBypetId(int petId) {
		Response Response = RestAssured
		           .given()
		           .param("petId", petId)
                   .log()
                .everything().delete("/pet"+"/"+petId);
		
		
		Response.prettyPrint();
	    System.out.println(Response.getStatusCode());
	    return Response;	
		
		
	}
}
