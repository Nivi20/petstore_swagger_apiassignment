package com.petstore.APITestingFramework.cuke.apis;
import org.testng.annotations.Test;

import com.petstore.APITesting.pojo.UserData;
import com.petstore.APITestingFramework.cuke.setUp.BaseTest;

//import java.net.http.HttpResponse.ResponseInfo;
import java.util.ArrayList;

import io.restassured.response.Response;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class UserAPI extends BaseTest {
   
  @Test
  public static Response createUser(UserData Data) {
    
          		
    		Response addResponse = (Response) RestAssured
            		.given()
                    .contentType(ContentType.JSON)
                    .body(Data)
                    .log()
                    .everything().post("/user");
                    
         addResponse.prettyPrint();
         System.out.println(addResponse.getStatusCode());
        // System.out.println("Response is: "+addResponse.asString());
        return addResponse;
                    
            		 }


	@Test
 public static Response CreateUserWithArray(String id,String username, String firstName, String lastName, String email, String password, String phone, String userStatus) {
	  ArrayList<UserData> list = new ArrayList<UserData>();
	  list.add(new UserData(id,username,firstName, lastName, email, password, phone, userStatus));
	  
	  Response response = RestAssured.given().contentType(ContentType.JSON)
              .body(list)
              .log()
              .everything().post("/user/createWithArray");
	  response.prettyPrint();
      System.out.println(response.getStatusCode());
	return response;
	  
	  
  }
	
	
	
public static Response UserLogsInTheSystem(String username,String password) {
		
		Response response = RestAssured
				            .given()
				           .param(username, password)
				           .log()
				           .everything()
				           .get("/user/login");
		response.prettyPrint();
	      System.out.println(response.getStatusCode());
		return response;
				            


}
public static Response GetUserData(String userName)
{	
	Response response = RestAssured
            .given()
            .log()
            .everything().get("/user/"+userName);
	response.prettyPrint();
	System.out.println(userName);
    System.out.println(response.getStatusCode());
	
	return response;
	
}


public static Response UpdateUserData(String id, String userName, String firstName, String lastName, String email, String password, String phone, String userStatus) {
	
	UserData Updatedata =  new UserData(id,userName, firstName, lastName, email, password, phone, userStatus);
	
	Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .body(Updatedata)
            .log()
            .everything().put("/user/"+userName);
	response.prettyPrint();
    System.out.println(response.getStatusCode());
	
	return response;
	
}

public static Response DeleteUserData(String userName) {
	
	Response response = RestAssured
            .given()
            .log()
            .everything().delete("/user/"+userName);
	response.prettyPrint();
    System.out.println(response.getStatusCode());
	
	return response;
}


public static Response LogoutUser() {

	
	Response response = RestAssured
            .given()
            .log()
            .everything().get("/user/logout");
	response.prettyPrint();
    System.out.println(response.getStatusCode());
	
	return response;
}
  }

