package com.petstore.APITestingFramework.cuke.apis;

import com.petstore.APITesting.pojo.StoreData;
//import com.petstore.APITesting.pojo.UserData;
import com.petstore.APITestingFramework.cuke.setUp.BaseTest;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class StoreAPI extends BaseTest {
	
	public static Response orderPet(StoreData Data) {
		    
   		
 		 Response =  RestAssured
         		           .given()
                            .contentType(ContentType.JSON)
                             .body(Data)
                            .log()
                            .everything().post("/store/order");
                 
      Response.prettyPrint();
      System.out.println(Response.getStatusCode());
     //System.out.println("Response is: "+Response.asString());
     String orderId = Response.jsonPath().get("id").toString();
    
		return Response;
		 }
	
	public static Response findPurchaseOrderById(String orderId) {
		
	
		Response =  RestAssured
				       .given()
				       .log()
				       .everything().get("/store/order"+"/"+orderId);
		
		  Response.prettyPrint();
	      System.out.println(Response.getStatusCode());
		
		return Response;
		
	}
	
	public static Response deletePurchaseOrderById(String orderId) {
		
		Response =  RestAssured
				    .given()
				    .log()
				    .everything().delete("/store/order"+"/"+orderId);
		
		Response.prettyPrint();
	      System.out.println(Response.getStatusCode());
		
		return Response;
		
	}
	
	public static Response returnsPetInventoriesByStatus() {
		
		Response =  RestAssured
			    .given()
			    .log()
			    .everything().get("/store/inventory");
		
		Response.prettyPrint();
	     System.out.println(Response.getStatusCode());
		return Response;
		
	}

	
}
