package com.petstore.APITestingFramework.cuke.Steps;



import org.testng.Assert;

//import com.petstore.APITesting.StoreAPI;
import com.petstore.APITesting.pojo.StoreData;
import com.petstore.APITestingFramework.cuke.apis.StoreAPI;
import com.petstore.APITestingFramework.cuke.setUp.BaseTest;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class storePetStepdefinition extends BaseTest {
	
	

	
	
	
	@When("place an order for purchasing pet {string},{string},{string},{string},{string},{string}")
	public void place_an_order_for_purchasing_pet(String id,String petId, String quantity,String shipDate,String status,String complete) {
		StoreData Data =  new StoreData(id,petId, quantity, shipDate, status, complete);
		BaseTest.Response=StoreAPI.orderPet(Data);
	}
	
	@When("User pass {string}")
	public void user_pass(String orderId) {
	
		BaseTest.Response=StoreAPI.findPurchaseOrderById(orderId);
			
		}

	

     @When("user send Petorder id {string}")
             public void user_send_Petorder_id(String orderId) {
	BaseTest.Response=StoreAPI.findPurchaseOrderById(orderId);
        }


	
		//Assert.assertEquals(BaseTest.Response.jsonPath().get("id").toString(), id);
		
	
	
	@Then("check if purchase order data is correct {string},{string},{string},{string},{string},{string}")
	public void check_if_purchase_order_data_is_correct(String id,String petId, String quantity,String shipDate,String status,String complete) {
		Assert.assertEquals(BaseTest.Response.jsonPath().get("id").toString(), id);
		Assert.assertEquals(BaseTest.Response.jsonPath().get("petId").toString(), petId);
		Assert.assertEquals(BaseTest.Response.jsonPath().get("quantity").toString(), quantity);
		Assert.assertEquals(BaseTest.Response.jsonPath().get("shipDate").toString(), shipDate);
		Assert.assertEquals(BaseTest.Response.jsonPath().get("status").toString(), status);
		Assert.assertEquals(BaseTest.Response.jsonPath().get("complete").toString(), complete);
	}
	
	@When("user delete {string}")
	public void user_delete(String orderId) {
	    BaseTest.Response=StoreAPI.deletePurchaseOrderById(orderId);
	}


 @Then("user request pet inventory for each status code")
   public void user_request_pet_inventory_for_each_status_code() {
    BaseTest.Response=StoreAPI.returnsPetInventoriesByStatus();
}




	
	
}
