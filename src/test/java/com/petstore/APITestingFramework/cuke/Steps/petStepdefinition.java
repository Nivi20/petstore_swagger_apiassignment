package com.petstore.APITestingFramework.cuke.Steps;

import java.util.ArrayList;


import com.petstore.APITesting.pojo.PetData;
//import com.petstore.APITesting.pojo.PetCategory;
import com.petstore.APITesting.pojo.PetTags;
//import com.petstore.APITesting.pojo.PetPhotoUrls;
//import com.petstore.APITesting.pojo.StoreData;
import com.petstore.APITestingFramework.cuke.apis.PetAPI;
import com.petstore.APITestingFramework.cuke.setUp.BaseTest;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
//import io.restassured.path.json.JsonPath;

public class petStepdefinition {
	



@When("User sends new pet details  to add to the store {int},{string},{string},{int},{string},{int},{string},{string}")
	public void user_sends_new_pet_details_to_add_to_the_store(int id, String name, String photoUrl, int catId, String catName, int tagId1, String tagName1, String status) {
	    

		ArrayList<String> urlList = new ArrayList<String>();
		urlList.add(new String(photoUrl));
		
		ArrayList<PetTags> tagData=new ArrayList<PetTags>();
		tagData.add(new PetTags(tagId1,tagName1));
		
		PetData Data= new PetData(id,name,urlList,catId,catName,tagData,status);
		BaseTest.Response = PetAPI.orderPet(Data);
		
		
	}





@Then("User updates pet details {int},{string},{string},{int},{string},{int},{string},{string}")
	public void user_updates_pet_details(int id, String name, String photoUrl, int catId, String catName, int tagId1, String tagName1, String status) {
		ArrayList<String> urlList = new ArrayList<String>();
		urlList.add(new String(photoUrl));
		
		ArrayList<PetTags> tagData=new ArrayList<PetTags>();
		tagData.add(new PetTags(tagId1,tagName1));
		
		PetData Data= new PetData(id,name,urlList,catId,catName,tagData,status);
		BaseTest.Response=PetAPI.updatePet(Data);
	}


@When("User send request to find pet by Status {string}")
	public void user_send_request_to_find_pet_by_Status(String status) {
	    BaseTest.Response = PetAPI.findPetByStatus(status);
	}

@When("User send request to find pet by {int}")
	public void user_send_request_to_find_pet_by(int petId) {
		BaseTest.Response =  PetAPI.findPetBypetId(petId);
	}


	

@When("User updates {int},{string}, {string}")
	public void user_updates(int petId, String name, String status) {
	    
		BaseTest.Response = PetAPI.updatePetBypetId(petId,name,status);
	}


@When("User send request to delete pet by {int}")
	public void user_send_request_to_delete_pet_by(int petId) {
		 BaseTest.Response = PetAPI.deletePetBypetId(petId);
	}




}
