package com.petstore.APITestingFramework.cuke.Steps;

import org.testng.Assert;
import org.testng.annotations.Test;

//import com.petstore.APITesting.CreateUserTest;

//import com.petstore.APITesting.UserAPI;
import com.petstore.APITesting.pojo.UserData;
import com.petstore.APITestingFramework.cuke.apis.UserAPI;
import com.petstore.APITestingFramework.cuke.setUp.BaseTest;

import io.restassured.response.Response;
import io.restassured.RestAssured;

//import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class createUserStepdefinition extends BaseTest {
	
	
@Given("When URI is available")
	public void when_URI_is_available() {
		BaseTest.setup();
	}

@Given("^User pass \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void _pass_Id_userName_firstName_lastName_Email_Password_Phone_userStatus(String id,String userName, String firstName,String lastName,String email,String password,String phone, String userStatus) {
		UserData Data =  new UserData(id,userName, firstName, lastName, email, password, phone, userStatus);
		BaseTest.Response=com.petstore.APITestingFramework.cuke.apis.UserAPI.createUser(Data);
	}

@Given("^User send \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
public void user_pass_in_array(String id,String userName, String firstName,String lastName,String email,String password,String phone, String userStatus) {
	BaseTest.Response=UserAPI.CreateUserWithArray(id, userName, firstName, lastName, email, password, phone, userStatus);
}

    
@When("User sends \"([^\"]*)\", \"([^\"]*)\"$")
public void user_sends(String username, String password) {
	BaseTest.Response=UserAPI.UserLogsInTheSystem(username,password);
}


@When("Users delete data \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
public void users_delete_data(String id,String userName, String firstName,String lastName,String email,String password,String phone, String userStatus) {
	BaseTest.Response=UserAPI.DeleteUserData(userName);
}



@Then("check if a new user is created {string}")
public void check_if_a_new_user_is_created(String id) {
    // Write code here that turns the phrase above into concrete actions
	 Assert.assertEquals(BaseTest.Response.jsonPath().get("message").toString(), id);
}

@Then("check if response status code is {string}")
public void check_if_response_status_code_is(String statusCode) {
    // Write code here that turns the phrase above into concrete actions
	Assert.assertEquals(BaseTest.Response.jsonPath().get("code").toString(), statusCode);
	
}


@Then("check if User has logged in the system")
public void check_if_User_has_logged_in_the_system() {
    // Write code here that turns the phrase above into concrete actions
	Assert.assertEquals(BaseTest.Response.jsonPath().get("message").toString().substring(0,22), "logged in user session");

}


@Then("check if response get status code {int}")
public void check_if_response_body_contains(int statusCode) {
    // Write code here that turns the phrase above into concrete actions
	Assert.assertEquals(BaseTest.Response.getStatusCode(), statusCode);
   
}


@When("the user logs in the system using {string},{string}")
public void the_user_logs_in_the_system_using(String username, String password) {
    // Write code here that turns the phrase above into concrete actions
	BaseTest.Response=UserAPI.UserLogsInTheSystem(username,password);
   
}


@When("^the user update the data \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
public void user_updates_data(String id,String userName, String firstName,String lastName,String email,String password,String phone, String userStatus) {
   BaseTest.Response = UserAPI.UpdateUserData(id, userName, firstName, lastName, email, password, phone, userStatus);
}



@Then("^the userdata is updated \"([^\"]*)\"$")
public void userdata_is_updated(String id) {
    // Write code here that turns the phrase above into concrete actions
	Assert.assertEquals(BaseTest.Response.jsonPath().get("code").toString(), "200");
	Assert.assertEquals(BaseTest.Response.jsonPath().get("message").toString(), id);
	
}

@When("the logged-in user gets other user details \"([^\"]*)\"$")
public void get_users_data(String userName) {
	BaseTest.Response=UserAPI.GetUserData(userName);
}



@Then("check if user data is correct {string},{string},{string},{string},{string},{string},{string},{string}")
public void check_if_user_data_is_correct(String id, String username, String firstName, String lastName, String email, String password, String phone, String userStatus) {
	System.out.println("Response is");
	System.out.println(BaseTest.Response.toString());
	Assert.assertEquals(BaseTest.Response.jsonPath().get("id").toString(), id);
	Assert.assertEquals(BaseTest.Response.jsonPath().get("username").toString(), username);
	Assert.assertEquals(BaseTest.Response.jsonPath().get("firstName").toString(), firstName);
	Assert.assertEquals(BaseTest.Response.jsonPath().get("lastName").toString(), lastName);
	Assert.assertEquals(BaseTest.Response.jsonPath().get("email").toString(), email);
	Assert.assertEquals(BaseTest.Response.jsonPath().get("password").toString(), password);
	Assert.assertEquals(BaseTest.Response.jsonPath().get("phone").toString(), phone);
	//Assert.assertEquals(BaseTest.Response.jsonPath().get("userStatus").toString(), userStatus);
}

@When("check if response body contains {string},{string},{string}")
public void check_if_response_body_contains(String ecode, String etype, String emessage) {
    // Write code here that turns the phrase above into concrete actions
	Assert.assertEquals(BaseTest.Response.jsonPath().get("code").toString(),ecode);
	Assert.assertEquals(BaseTest.Response.jsonPath().get("type").toString(), etype);
	Assert.assertEquals(BaseTest.Response.jsonPath().get("message").toString(), emessage);
}	


@When("user logs out")
public void user_logouts() {
    BaseTest.Response = UserAPI.LogoutUser();
}

@When("the user delete the data {string}")
public void the_user_delete_the_data(String userName) {
	BaseTest.Response = UserAPI.DeleteUserData(userName);
}

}